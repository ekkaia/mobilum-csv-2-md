import csv from "csv-parser";
import { createReadStream, writeFile } from "fs";

// Pourcentage de progression dans l'import
let percent = null;

/**
 * Charge un CSV et retourne son équivalent en JSON
 * @param {string} csvName Nom du fichier CSV
 * @returns Promesse JSON du CSV parsé
 */
async function dataLoader(csvName) {
	console.log(`Chargement des données ${csvName}...`);
	return new Promise((resolve, reject) => {
		let csvdata = [];
		createReadStream(`data/${csvName}.csv`)
			.pipe(csv({ separator: "," }))
			.on("data", (d) => csvdata.push(d))
			.on("end", () => {
				resolve(csvdata);
			})
			.on("error", (err) => reject(err));
	});
}

function normalizeString(str) {
	return str.trim().normalize("NFD").replace(/\p{Diacritic}/gu, "").toLowerCase().replaceAll(" ", "-");
}

/**
 * Génère les fichiers Markdown
 * @param {Object} data JSON du CSV parsé
 */
async function generateMd(data) {
	console.log("Démarrage du script de gébération des fichiers Markdown...");
	// Parcours des données
	const promises = data.map((el, i) => {
		// Progression de l'import du CSV
		const progress = Math.floor(100 * i / data.length);

		if (percent !== progress) {
			console.info(`${progress}% complete...`);
			percent = progress;
		}

		return new Promise(function (resolve, reject) {
			const subCategory = normalizeString(el.sub_category);
			const gamme = `-${normalizeString(el.gamme)}`;
			const finitions = el.finitions ? `-${normalizeString(el.finitions)}` : "";
			writeFile(`dist/${subCategory}${gamme}${finitions}.md`, formatToMarkdown(el), (err) => {
				if (err) {
					reject(err);
				} else {
					resolve(data);
				}
			});
		});
	});
	await Promise.all(promises);
}

function formatToMarkdown(el) {
	let content = `---\n`;

	for (const [key, value] of Object.entries(el)) {
		content += `${key}: ${value}\n`;
	}

	content += `---\n`;

	return content;
}

// Fonction qui se lance à l'import
(async () => {
	try {
		// Calcul du temps d'import
		console.time("Import time:");

		const csv = await dataLoader("mobilum");
		await generateMd(csv);

		console.timeEnd("Import time:");
		console.log("Import status : success 🥳");
		process.exit(0);
	} catch (error) {
		console.log("Import status : failed 🤬");
		console.error(error);
		process.exit(1);
	}
})();

